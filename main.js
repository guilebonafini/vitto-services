const path = require('path');
const hostile = require('hostile');
const fs = require('fs');

let serverConfig = JSON.parse(fs.readFileSync(path.normalize(path.join(__dirname, '/servers.json'))));

//Configuring hosts
serverConfig.services.map((service) => {
    var url = serverConfig.template.url.replace('*', service.alias);
    console.log(`${url} [info]: Configuring host....`);
    hostile.set('127.0.0.1', url);
});

//Configuring environment

let processVariables = (variables) => variables.map((v) => { 
    switch(serverConfig.server_output) {
        case 'apache':
            return `SetEnv ${v.key} ${v.value}`;
        case 'nginx':
            return `fastcgi_param ${v.key} ${v.value}`;
        default:
            return '';
    }
}).join('\n');

let default_variables = processVariables(serverConfig.variables);


const { 
    template: confTemplateFile,
    output_file: confDefaultOutput,
    conf_path: confDefaultDir
} = serverConfig.server[serverConfig.server_output];

const confDefaultTemplate = String(fs.readFileSync(confTemplateFile));

//Starting php web servers
serverConfig.services.map((service) => { 
    
    var confTemplate = confDefaultTemplate;
    var confOutput = confDefaultOutput;
    var confDir = confDefaultDir;

    if(service.server) {
        confTemplate = String(fs.readFileSync(service.server[serverConfig.server_output].template));
        confOutput = service.server[serverConfig.server_output].output_file || confDefaultOutput;
        confDir = service.server[serverConfig.server_output].conf_path || confDefaultDir;
    }

    var variables = service.variables ? processVariables(service.variables) : default_variables;
    
    var url = service.url || serverConfig.template.url.replace('*', service.alias);
    var outputConfFile = path.normalize(path.join(confDir, confOutput.replace('*', url)));
    var servicePath = path.normalize(path.join(service.root || serverConfig.root, service.path));

    console.log(`${url} [info]: Creating file...`);

    fs.writeFile(
        outputConfFile, 
        confTemplate.replace(/{variables}/g, variables).replace(/{path}/g, servicePath).replace(/{url}/g, url).replace(/{alias}/g, service.alias),
        (err) => {
            if(err)
                return console.log(`${url} [error]: `, err);
            console.log(`${url} [info]: File created!`);        
        }
    );
});